# Clientes

Por defecto se integro el archivo 'install.sh' que ya tiene todo lo antes mencionado, solo basta con abrir la terminal y escribir los siguientes comandos:

```
cd script
./install.sh
```


## software necesario

```
sudo apt-get update
sudo apt-get install mplayer ristretto crontab ssh zenity
```


- **mplayer**:     Permite reproducir videos en pantalla de bloqueo
- **ristretto**:   Permite poner imágenes en pantalla de bloqueo
- **crontab**:     Permite establecer acciones programadas
- **ssh**:         Permite la conexión remota entre cliente servidor
- **zenity**:      Permite lanzar cuadros de dialogo desde la terminal

## Identificar dispositivos de entrada (opcional, si los archivos están por defecto bloquearan todo y desbloquearán todo.)

Para saber el id del teclado y ratón utiliza el siguiente comando

`xinput --list`

Busca y selecciona los id=# (por ejemplo: id=8) para seleccionar los periféricos USB/PS2 que se apagaran a la hora de bloquear los equipos.

Archivos de bloqueo y desbloqueo:

- unlock.sh
- lock.sh


## Programar acciones (opcional)

Para agregar mensajes automáticos utiliza 

`crontab -e `

toma en cuenta los siguientes parámetros

minuto(0-59) hora(0-23) día(1-31) mes(1-12) día de la semana(0=domingo) 

crontab -e

```
* 58 21 * * * ~/script/time.sh
* 00 22 * * * ~/script/shutdown.sh
```


## Para agregar videos o imágenes a la pantalla de bloqueo

Active la opción de mostrar archivos ocultos
(puede presionar ctrl + h o alt + .)

Guarde los archivos en la carpeta correspondiente dentro de la carpeta ~/.script/.bloqueo

Dentro del archivo lock.sh quite el símbolo "#" dependiendo de su elección.

* **mplayer** 	    para bloquear con vídeos
* **ristretto** 	    para bloquear con imágenes

## Orden de instalación (Para no complicarlo innecesariamente)

- Instalación del cliente
- Configurar SSH para clave pública/privada
    - Hacer prueba de conexión con todos los clientes (por ejemplo: ssh phablet@192.168.1.10) des del servidor.
    - Probar todas las funciones de CiberBiblio en el servidor.
- Instalar el administrador de pantalla **LightDM**
    - sudo apt install lightdm
- Cambiar el administrador de pantalla de **GDM** a **LightDM**
- Habilitar la sesión de invitado
```
sudo sh -c 'printf "[Seat:*]\nallow-guest=true\n" > /etc/lightdm/lightdm.conf.d/40-enable-guest.conf'
reboot
```
- Iniciar sesión con la sesión de invitado
    - Configurar el escritorio del invitado tal y como quieres que lo vean (Fondo de pantalla, dash ...).
    - Programar los scripts para que se inicien cada vez que se reinicie/encienda el cliente con la sesión de invitado:
        - .script/killZenity.sh (Opcional, cierra la primera ventana de aviso)
        - .script/lock.sh
    - Sin cerrar sesión cambiar a administrador y copiar el contenido de /temp/"Guest-Lo que sea" a /etc/guest-session/skel/ (Puedes utilizar `sudo nautilus`)
- Reiniciar y ver si se muestra el escritorio tal y como lo queremos y se bloquea el cliente.
- Habilitar el inicio de sesión de invitado por defecto.
    - Mover archivo lightdm.conf a /etc/lightdm/ (`sudo cp lightdm.conf /etc/lightdm/lightdm.conf`)
- Reiniciar
