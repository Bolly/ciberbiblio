#!/bin/bash

sudo apt-get update

sudo apt-get install zenity -y

zenity --info --window-icon="info" --text="Actualizacion de repositorios"

sudo apt-get update

zenity --info --window-icon="info" --text="Instalando aplicaciones"

sudo apt-get install mplayer ristretto cron ssh -y

zenity --info --window-icon="info" --text="Copiando archivos y modificando permisos de ejecución"

cp -rf script ~/.script
chmod u+x ~/.script/*.sh

zenity --info \
--text="Ahora puede reiniciar su equipo"
