# Bienvenido a CiberBiblio

Este proyecto es un fork de [ERAM](https://github.com/KajiiNarumi/ERAM), actualmente está en "construcción" por lo que hay archivos de **ERAM** que no se utilizan pero que no se borran por si los necesitamos más adelante.

## CiberBiblio v.0.1

**Funcionalidades**:
- Encendido de ordenadores.
- Reinicio de ordenadores.
- Apagado de ordenadores.
- Quitar bloqueo de pantalla, ratón y teclado cuando se da al botón "Desbloquear".
- Posibilidad de añadir más tiempo.
- Reinicio de ordenadores una vez se termina el tiempo.
- Código de colores en el tiempo:
    - Verde: Más de 5 minutos
    - Naranja: Menos de 5 minutos
    - Rojo: Fin del tiempo.
- Código de colores si devuelve ping en el nombre del PC
    - Azul: Devuelve ping (PC encendido y preparado)
    - Negro: No devuelve ping (PC apagado o no preparado)

Funciones administrativas:
- Encender, apagar, reiniciar, bloquear y desbloquear todos los equipos.

# Cómo funciona

**CiberBiblio** es un fork de **ERAM** escrito en **Phyton** para gestionar por ejemplo los ordenadores públicos de una biblioteca bajo sistemas **GNU/Linux**.

- Los ordenadores de los usuarios se iniciarán bloqueados y se irán desbloqueando mediante el programa.
- Los ordenadores de los usuarios se iniciarán cada vez con una cuenta de invitado diferente creada en la carpeta /temp.
- El tiempo por defecto es de 60 minutos pudiendo añadir tiempo extra durante cualquier momento (15-30-60)
- Cuando queden 5 minutos para que se termine el tiempo se mostrará un mensaje avisando al usuario.
- Cuando se termine el tiempo el ordenador cliente se reiniciará.
    - Al reiniciar se borrará /temp incluido la home de los invitados.
    - Se iniciará con otro usuario invitado con nombre aleatorio y bloqueado.

# Requisitos

Tal como está montado se necesitan los siguientes requisitos, aunque siempre se puede modificar un poco para que funcione en otros escenarios, imaginación al poder!

## Servidor (Sin requisitos conocidos)

## Clientes

- **Ubuntu 20.04 FOCAL FOSSA**
- Administrador de pantalla **LightDM** (Permite invitado)

## Red y SSH

- IP's fijas
- **SSH** por clave pública/privada, deshabilitar por contraseña.

# Orden de instalación (Para no complicarlo innecesariamente)

> En el directorio del clientes y el servidor hay instrucciones más detalladas del proceso.

- Instalación del servidor
- Instalación del cliente
- Habilitar IPs fijas
- Averiguar MACs de los clientes
- Configurar SSH para clave pública/privada
   - Hacer prueba de conexión con todos los clientes (por ejemplo: ssh phablet@192.168.1.10)
- Probar todas las funciones de **CiberBiblio** en el servidor
- Instalar el administrador de pantalla **LightDM**
- Cambiar de **GDM** a **LightDM**
- Habilitar la sesión de invitado
- Iniciar sesión con la sesión de invitado
    - Configurar el escritorio del invitado tal y como quieres que lo vean.
    - Programar los scripts para que se inicien cada vez que se reinicie/encienda el cliente:
        - .script/killZenity.sh
        - .script/lock.sh
    - Sin cerrar sesión cambiar a administrador y copiar el contenido de _/temp/"Guest-Lo que sea"_ a _/etc/guest-session/skel/_
- Reiniciar y ver si se muestra el escritorio tal y como lo queremos y se bloquea el cliente.
- Habilitar el inicio de sesión de invitado por defecto.

### LICENSE GPL v3 

Estamos trabajando bajo la licencia **GPL v3**, así que siéntase libre de estudiar y modificar este software.

### Cómo obtener ayuda, colaborar o reportar bugs

Puede obtener ayuda, colaborar o reportar bugs mediane la plataforma GitLab o en el siguiente [grupo de Telegram](https://t.me/CiberBiblio).
