from contextlib import nullcontext
import tkinter as Tkinter  
from datetime import datetime
import subprocess
import os

counter=[]
running=[]
numeroPc=0
ips=[]
macs=[]
online=[]

tiempoDefecto=3600
avisoFinal=301
tiempoReinicio=-40
tiempoEncendido=-5
tiempoApagado=-25

mensaje1=[]
mensaje2=[]


archivo=open('/home/biblioteca/.Biblioteca/PCIPMAC.txt','r',encoding="utf-8")
for linea in archivo.readlines():
    cortada = linea.split()
    counter.append(tiempoDefecto)
    running.append(False)
    ips.append (cortada[4])
    macs.append (cortada[3])
    numeroPc=numeroPc+1
    mensaje1.append ("Reseteando")
    mensaje2.append ("00:00:00")
archivo.close()

def encenderTodos():
    for x in range(numeroPc):
        subprocess.run(['wakeonlan',macs[x]])

def apagarTodos():
    for x in range(numeroPc):
        subprocess.Popen(['ssh', 'informatica@'+ips[x], 'sudo shutdown now'])

def desbloquearTodos():
    for x in range(numeroPc):
        guest=subprocess.getoutput("ssh informatica@"+ips[x]+" who | grep -v pts | awk '{print $1}'| grep -v informatica | sed 's/$/@/g'")
        subprocess.Popen(['ssh', guest+ips[x], './.script/unlock.sh'])

def bloquearTodos():
    for x in range(numeroPc):
        guest=subprocess.getoutput("ssh informatica@"+ips[x]+" who | grep -v pts | awk '{print $1}'| grep -v informatica | sed 's/$/@/g'")
        subprocess.Popen(['ssh', guest+ips[x], './.script/lock.sh'])

def resetearTodos():
    for x in range(numeroPc):
        subprocess.Popen(['ssh', 'informatica@'+ips[x], 'sudo reboot'])

def counter_label(labelCount,x):
    def count():
        if running[x]:
            global counter
            tt = datetime.fromtimestamp(counter[x])
            string = tt.strftime("%-H:%M:%S") 
            display=string  
            labelCount.after(1000, count)
            counter[x] -= 1
            if x==0:
                archivo=open('/home/biblioteca/.Biblioteca/IPs.txt','r',encoding="utf-8")
                y=0
                for linea in archivo.readlines():
                    cortada = linea.split()
                    online.append (cortada[0])
                    if online[y] == "True":
                        exec('labelPc{}["fg"]="blue"'.format(y))
                    else:
                        exec('labelPc{}["fg"]="black"'.format(y))
                    y+=1
                archivo.close()
                del online[:]
            if avisoFinal<counter[x]:
                labelCount['fg']="green"
            elif avisoFinal==counter[x]+1:
                labelCount['fg']="orange"
                guest=subprocess.getoutput("ssh informatica@"+ips[x]+" who | grep -v pts | awk '{print $1}'| grep -v informatica | sed 's/$/@/g'")
                subprocess.Popen(['ssh', guest+ips[x], './.script/time.sh'])
            elif counter[x]==-1:         
                display=mensaje1[x]
                labelCount['fg']="red"
                Reset(labelCount,x)
            elif counter[x]<=-2 and counter[x]>=tiempoReinicio+1:
                if counter[x]%2:
                    display=mensaje1[x]
                else:
                    display=mensaje2[x]
            elif counter[x]==tiempoReinicio:
                display=mensaje1[x]
                running[x]=False
            
        else:
            labelCount['fg']="black"
            display="1:00:00"
            counter[x]=tiempoDefecto
            habilitarBotones(x)

        labelCount['text']=display+" "
    count()

def anadirTiempo(labelCount,x,masTiempo):
    counter[x]+=masTiempo
    tt = datetime.fromtimestamp(counter[x])
    string = tt.strftime("%-H:%M:%S")
    display=string  
    labelCount['text']=display+" " 
    
def Start(labelCount,x): 
    global running
    running[x]=True
    counter_label(labelCount,x)
    exec("start{}['state']='disabled'".format(x))
    exec("stop{}['state']='normal'".format(x))
    exec("reset{}['state']='normal'".format(x))
    guest=subprocess.getoutput("ssh informatica@"+ips[x]+" who | grep -v pts | awk '{print $1}'| grep -v informatica | sed 's/$/@/g'")
    print("hola")
    subprocess.Popen(['ssh', guest+ips[x], './.script/unlock.sh'])
    
def Stop(x):
    global running
    exec("start{}['state']='normal'".format(x))
    exec("stop{}['state']='disabled'".format(x))
    exec("reset{}['state']='normal'".format(x))
    exec("running[{}] = False".format(x))
    guest=subprocess.getoutput("ssh informatica@"+ips[x]+" who | grep -v pts | awk '{print $1}'| grep -v informatica | sed 's/$/@/g'")
    subprocess.Popen(['ssh', guest+ips[x], './.script/lock.sh'])
    
def Reset(labelCount,x):
    global counter
    if running[x]==False:
        running[x]=True
        counter_label(labelCount,x)
        labelCount['fg']="blue"
    if counter[x]>0:
        counter[x]=-1
        mensaje2[x]="Espere"
    else:
        mensaje2[x]="00:00:00"
    deshabilitarBotones(x)
    subprocess.Popen(['ssh', 'informatica@'+ips[x], 'sudo reboot'])

def Encender(labelCount,x):
    if running[x]==False:
        running[x]=True
        counter_label(labelCount,x)
    counter[x]=tiempoEncendido
    labelCount['fg']="blue"
    mensaje1[x]="Encendiendo"
    mensaje2[x]="Espere"
    deshabilitarBotones(x)
    subprocess.run(['wakeonlan',macs[x]])

def Apagar(labelCount,x):
    if running[x]==False:
        running[x]=True
        counter_label(labelCount,x)
    counter[x]=tiempoApagado
    labelCount['fg']="blue"
    mensaje1[x]="Apagando"
    mensaje2[x]="Espere"
    deshabilitarBotones(x)
    subprocess.Popen(['ssh', 'informatica@'+ips[x], 'sudo shutdown now'])

def habilitarBotones(x):
    exec("t15{}['state']='normal'".format(x))
    exec("t30{}['state']='normal'".format(x))
    exec("t60{}['state']='normal'".format(x))
    exec("start{}['state']='normal'".format(x))
    exec("stop{}['state']='disabled'".format(x))
    exec("reset{}['state']='normal'".format(x))
    exec("encender{}['state']='normal'".format(x))
    exec("apagar{}['state']='normal'".format(x))

def deshabilitarBotones(x):
    exec("t15{}['state']='disabled'".format(x))
    exec("t30{}['state']='disabled'".format(x))
    exec("t60{}['state']='disabled'".format(x))
    exec("start{}['state']='disabled'".format(x))
    exec("stop{}['state']='disabled'".format(x))
    exec("reset{}['state']='disabled'".format(x))
    exec("encender{}['state']='disabled'".format(x))
    exec("apagar{}['state']='disabled'".format(x))

root = Tkinter.Tk()
root.title("CiberBiblio v.0.1")
    
root.minsize(width=500, height=200)


f = Tkinter.Frame(root)
encenderTodos = Tkinter.Button(f, text='Encender todos', width=14, borderwidth=2, command=encenderTodos)
apagarTodos = Tkinter.Button(f, text='Apagar todos', width=14, borderwidth=2, command=apagarTodos)
desbloquearTodos = Tkinter.Button(f, text='Desbloquear todos', width=14, borderwidth=2, command=desbloquearTodos)
bloquearTodos = Tkinter.Button(f, text='Bloquear todos', width=14, borderwidth=2, command=bloquearTodos)
resetearTodos = Tkinter.Button(f, text='Resetear todos', width=14, borderwidth=2, command=resetearTodos)


f.pack(pady=20)
encenderTodos.pack(side="left", padx=1)
apagarTodos.pack(side="left", padx=1)
desbloquearTodos.pack(side="left", padx=1)
bloquearTodos.pack(side="left", padx=1)
resetearTodos.pack(side="left", padx=1)

for x in range(numeroPc):
    f = Tkinter.Frame(root)
    if x<10:
        exec('labelPc{} = Tkinter.Label(f, text="PC 0"+str(x)+" -> ", fg="black", font="Verdana 20 bold")'.format(x))
    else:
        exec('labelPc{} = Tkinter.Label(f, text="PC "+str(x)+" -> ", fg="black", font="Verdana 20 bold")'.format(x))
    exec('labelCount{} = Tkinter.Label(f, text="1:00:00"+" ", fg="black", width=10, font="Verdana 20 bold")'.format(x))
    exec("t15{} = Tkinter.Button(f, text='15',width=6, command=lambda:anadirTiempo(labelCount{},{},900))".format(x,x,x))
    exec("t30{} = Tkinter.Button(f, text='30',width=6, command=lambda:anadirTiempo(labelCount{},{},1800))".format(x,x,x))
    exec("t60{} = Tkinter.Button(f, text='60',width=6, command=lambda:anadirTiempo(labelCount{},{},3600))".format(x,x,x))
    exec("start{} = Tkinter.Button(f, text='Desbloquear', width=10, command=lambda:Start(labelCount{},{}))".format(x,x,x))  
    exec("stop{} = Tkinter.Button(f, text='Bloquear',width=6,state='disabled', command=lambda:Stop({}))".format(x,x))
    exec("reset{} = Tkinter.Button(f, text='Resetear',width=6, command=lambda:Reset(labelCount{},{}))".format(x,x,x))
    exec("encender{} = Tkinter.Button(f, text='Encender',width=6, command=lambda:Encender(labelCount{},{}))".format(x,x,x))
    exec("apagar{} = Tkinter.Button(f, text='Apagar',width=6, command=lambda:Apagar(labelCount{},{}))".format(x,x,x))
    f.pack(anchor = 'w',padx=10,pady=5)
    if (x>0):
        exec('labelPc{}.pack(side="left")'.format(x))
        exec('labelCount{}.pack(side="left")'.format(x))
        exec('t15{}.pack(side="left")'.format(x))
        exec('t30{}.pack(side="left")'.format(x))
        exec('t60{}.pack(side="left")'.format(x))
        exec('start{}.pack(side="left")'.format(x)) 
        #exec('stop{}.pack(side ="left")'.format(x))   
        exec('reset{}.pack(side="left")'.format(x))   
        exec('encender{}.pack(side="left")'.format(x))   
        exec('apagar{}.pack(side="left")'.format(x))
    else:
        counter[x]=82800 # 23 horas
        running[x]="True"
exec("counter_label(labelCount{},{})".format(0,0))
root.mainloop()