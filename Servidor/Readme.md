# Servidor

Por defecto se integro el archivo 'install.sh' que ya tiene todo lo antes mencionado, solo basta con abrir la terminal y escribir los siguientes comandos:

```
cd script
./install.sh
```



## Software necesario

```
sudo apt-get update
sudo apt-get install ssh nmap wakeonlan
```



- **ssh**: permite la conexión remota entre cliente servidor.
- **wakeonlan**: permite el encendido de ordenadores.

## Identificar dispositivos en red

Utilice el siguiente comando para buscar los equipos conectados en su red y poder obtener la información de las MAC.

`sudo nmap -sP x.x.x.x-x`

Establezca el rango de su ip

192.168.0.1-254

aquí escanea de la ip 192.168.0.1 a la 192.168.0.254

## Orden de instalación (Para no complicarlo innecesariamente)

- Instalación del servidor
- Habilitar IP's fijas i averiguar MACs de los clientes
    - Copiar los datos (IPs y MACs) en el archivo PCIPMAC.txt (Mantenga el formato!, puedes añadir/borrar filas sin problemas)
- Configurar SSH por clave pública/privada deshabilitando la conexión por contraseña.
   - Hacer prueba de conexión con con todos los clientes (por ejemplo: ssh phablet@192.168.1.10)
- Programar el script y el programa para que se inicien cada vez que se reinicie/encienda el servidor:
    - ~/.Biblioteca/ScanLAN.sh
    - ~/Escritorio/CiberBiblio.sh
- Probar todas las funciones de CiberBiblio en el servidor (Es necesaria la instalación del cliente)
